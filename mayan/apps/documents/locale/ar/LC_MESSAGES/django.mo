��    B      ,  Y   <      �     �  5   �  A   �  F        d  s   m     �     �     �               3     D  
   a     l     t     {     �     �     �     �  &   �  	   �     �     �  
             !     :     N     ]  N   e  $   �  o   �      I	     j	     |	     �	     �	  	   �	     �	     �	  	   �	  U   �	  V   
     t
  8   y
  
   �
  
   �
     �
     �
     �
  &   �
       y     (   �     �     �     �     �     �     �     �       2     �  E  
     a     S     j   �     >  |   G  
   �     �      �     �  $        9  1   O     �     �     �      �     �     �     �     �  4        I  
   X     c     }  
   �  $   �  $   �     �       Z     0   j  �   �  /   o     �     �     �  '   �     �          #     2  k   D  i   �       M   &     t     �  
   �  /   �  
   �  +   �  
     �     9   �      �        
   .     9     >     F      S     t  M   �     >   	          8          '           $   ?   &       9           <              B       :                     0   (   *                          7   .   -   4           )       ,             +   
   "      =          3   6   2   @   5                        %   A         !                            #   /   1      ;       Add All later version after this one will be deleted too. Amount in degrees to rotate a document page per user interaction. Amount in percent zoom in or out a document page per user interaction. Checksum Clear the graphics representations used to speed up the documents' display and interactive transformations results. Comment Compress Compressed filename Create a document type Create document types Create documents Create new document versions Date added Default Delete Delete document types Delete documents Description Details Document type Document version reverted successfully Documents Download Download documents Duplicates Edit Edit document properties Edit document types Edit documents Enabled Error deleting the page transformations for document: %(document)s; %(error)s. Error reverting document version; %s Every uploaded document must be assigned a document type, it is the basic way Mayan EDMS categorizes documents. Execute document modifying tools Exists in storage File File mimetype File path in storage File size Filename Label MIME type Maximum amount in percent (%) to allow user to zoom in a document page interactively. Minimum amount in percent (%) to allow user to zoom out a document page interactively. None Page %(page_num)d out of %(total_pages)d of %(document)s Page image Page range Pages Quick document rename Remove Revert documents to a previous version Submit The filename of the compressed file that will contain the documents to be downloaded, if the previous option is selected. There are no more pages in this document Total documents Total pages Type UUID Unknown User View document types View documents You are already at the first page of this document Project-Id-Version: Mayan EDMS
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2019-03-16 22:55+0000
Last-Translator: Yaman Sanobar <yman.snober@gmail.com>
Language-Team: Arabic (http://www.transifex.com/rosarior/mayan-edms/language/ar/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: ar
Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 && n%100<=10 ? 3 : n%100>=11 && n%100<=99 ? 4 : 5;
 إضافة سيتم حذف جميع الإصدارات اللاحقة بعد هذا الإصدار أيضا. الدرجة المسموح بها لتدوير الوثيقة لكل مستخدم. النسبة المئوية لتكبير أو تصغير في صفحة الوثيقة لكل مستخدم. Checksum مسح بيانات الرسومات المستخدمة لتسريع عرض الوثائق و نتائج التحويلات. تعليق ضغط اسم الملف المضغوط انشاء نوع وثيقة إنشاء أنواع الوثائق إنشاء وثائق انشأ اصدارات جديدة للوثيقة تاريخ الاضافة Default حذف حذف أنواع الوثائق حذف الوثائق Description التفاصيل نوع الوثيقة تم ارجاع اصدار الوثيقة بنجاح الوثائق تحميل تحميل الوثائق النسخ المكررة تحرير تحرير خصائص الوثيقة تحرير أنواع الوثائق تحرير الوثائق مفعل خطأ بمسح التحويلات الخاصة بالوثيقة: %(document)s; %(error)s. خطأ بارجاع اصدار الوثيقة %s كل وثيقة ترفع يجب ان يحدد لها نوع "نوع الوثيقة", انها الطريقة الاساسية التي يقوم مايان اي دي ام اس بها بترتيب الوثائق.  تشغيل أدوات تعديل الوثيقة موجود في التخزين ملف نوع الملف مسار الملف في التخزين حجم الملف اسم الملف العنوان نوع الملف أكبر نسبة مئوية (%) للسماح بالتكبير داخل الوثيقة لكل مستخدم. أقل نسبة مئوية (%) للسماح بالتكبير داخل الوثيقة لكل مستخدم. لا شيء صفحة %(page_num)d من أصل %(total_pages)d للوثيقة %(document)s صورة الصفحة نطاق الصفحات صفحات إعادة تسمية الوثيقة بسرعة إزالة أعد الوثيقة لإصدار قديم ارسال اسم الملف المضغوط سيحتوي الوثائق التي سيتم تحويلها، اذا تم اختيار الخيار السابق لا توجد صفحات أخرى بهذه الوثيقة عدد الوثائق الكلي عدد الصفحات الكلي النوع UUID Unknown مستخدم عرض أنواع الوثائق عرض الوثائق أنت بالفعل في الصفحة الأولى من هذه الوثيقة 